import express from "express";
import logger from 'morgan' 
import "dotenv/config"
const port = process.env.PORT || 2000

const app = express()
app.use(logger('dev'))

app.get('/', (req, res) => {
    res.send('Esto es el chat')
})

app.listen(port, () => {
    console.log(`Server running on http://localhost:${port}`)
})